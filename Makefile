PDF_SOURCES = $(shell find . -name "*.tex")
TARGETS = $(PDF_SOURCES:.tex=.pdf)

.PHONY: all
all: $(TARGETS)

$(TARGETS) : $(PDF_SOURCES)

%.pdf: %.tex
	@pdflatex -interaction nonstopmode -halt-on-error -file-line-error $*
	@rm -f *.log *.aux $*.ilg $*.ind $*.toc $*.bbl $*.blg $*.out *.asc *.bcf $*.run.xml

.PHONY: clean
clean:
		@rm *.pdf
